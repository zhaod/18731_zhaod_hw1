#ifndef CLICK_STATEFULFIREWALL_HH
#define CLICK_STATEFULFIREWALL_HH
#include <click/element.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <click/args.hh>
#include <map>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
//CLICK_DECLS

using namespace std;

/* Handshake status */
#define SYN 0
#define SYNACK 1
#define HS_DONE 2

class Connection{
private:
	String sourceip;
	String destip;
	int sourceport;
	int destport;
	int proto;
	unsigned long sourceseq;
	unsigned long destseq;
    unsigned long ack;
	int handshake_stat;
	bool isfw; //true if forward connection. false if reverse connection.
public:
	Connection(String s, String d, int sp, int dp, unsigned long seq_s, unsigned long seq_d,
               unsigned long ack_num, int pr, bool fwdflag)
	{
		//Add your implementation here.
		sourceip = s;
		destip = d;
		sourceport = sp;
		destport = dp;
		proto = pr;
		sourceseq = seq_s;
		destseq = seq_d;
		isfw = fwdflag;
		handshake_stat = SYN;
        // Use to judge if reverse direction flow is old connection
        ack = ack_num;
	}
	Connection()
	{
	}
	~Connection()
	{

	}

	/* Can be useful for debugging*/
	void print() const
	{
		//Add your implementation here.
		click_chatter("Connection: (%s, %d) -> (%s, %d) proto %d, src_seq %lu, dst_seq %lu, ack %u, hs_state %d",
			   sourceip.c_str(), sourceport, destip.c_str(), destport, proto, sourceseq, destseq, ack, handshake_stat);
	}

	/* Overlaod == operator to check if two Connection objects are equal.
	 * You may or may not want to ignore the isfw flag for comparison depending on your implementation.
	 * Return true if equal. false otherwise. */
	bool operator==(const Connection &other) const
	{
		// Check protocol mismatch
        if (other.proto != proto)
        {
            return false;
        }

		// Check ip: port, same direction
		if (other.sourceip == sourceip and other.sourceport == sourceport)
			return true;

		return is_reverse(other);
	}

	/*Compare two connections to determine the sequence in map.*/
	int compare(const Connection other) const
	{
		if (other.sourceip == sourceip) {
            if (other.destip == destip) {
                if (other.sourceport == sourceport) {
                    if(other.destport == destport) {
                        // Exactly equal
                        if (other.proto == proto) { return 0; }
                        else {
                            if (proto < other.proto) return -1;
                            else return 1; }
                    } else {
                        if (destport < other.destport) return -1;
                        else return 1; }
                } else {
                    if (sourceport < other.sourceport) return -1;
                    else return 1; }
            } else {
                if (destip < other.destip) return -1;
                else return 1; }
        } else {
            if (sourceip < other.sourceip) return -1;
            else return 1; }
	}

    bool is_reverse(const Connection other) const
    {
        return (other.sourceip == destip and other.destip == sourceip
				and other.sourceport == destport and other.destport == sourceport);
    }

    unsigned long get_sourceseq() const
	{
		return sourceseq;
	}

	unsigned long get_destseq() const
    {
		return destseq;
	}

	void set_sourceseq( unsigned long seq_s )
	{
		sourceseq = seq_s;
	}

	void set_destseq( unsigned long seq_d )
	{
        destseq = seq_d;
	}

    unsigned long get_ack()
    {
        return ack;
    }

    void reset_ack()
    {
        ack = 0;
    }

    int get_handshake_stat() const
	{
		return handshake_stat;
	}

	/* Update the status of the handshake */
	const void update_handshake_stat(int state)
	{
		handshake_stat = state;
	}

	/* Return value of isfw*/
	bool is_forward() { return isfw; }
};

class Policy{
private:
	String sourceip;
	String destip;
	int sourceport;
	int destport;
	int proto;
	int action;
public:
	Policy(String s, String d, int sp, int dp, int p, int act)
	{
		//Add your implementation here.
		sourceip = s;
		destip = d;
		sourceport = sp;
		destport = dp;
		proto = p;
		action = act;

	}
	~Policy() {}
	/* Return a Connection object representing policy */
	Connection getConnection();
	/* Return action for this Policy */
	int getAction() { return action; }
};

struct cmp_connection
{
	bool operator()(Connection const a, Connection const b)
	{
		return a.compare(b) > 0;
	}
};

class StatefulFirewall : public Element {
private:
	std::map<Connection,int, cmp_connection> Connections; //Map of connections to their actions.
	std::vector<Policy> list_of_policies;
public:
	StatefulFirewall()
	{
	}

	~StatefulFirewall()
	{
	}

	/* Take the configuration paramenters as input corresponding to
     * POLICYFILE and DEFAULT where
     * POLICYFILE : Path of policy file
     * DEFAULT : Default action (0/1)
     *
     * Hint: Refer to configure methods in other elemsnts.*/
	int configure(Vector<String> &conf, ErrorHandler *errh);

	const char *class_name() const		{ return "StatefulFirewall"; }
	const char *port_count() const		{ return "1/2"; }
	const char *processing() const		{ return PUSH; }
	// this element does not need AlignmentInfo; override Classifier's "A" flag
	const char *flags() const			{ return ""; }

	/* return true if Packet represents a new connection
     * i.e., check if the connection exists in the map.
     * You can also check the SYN flag in the header to be sure.
     * else return false.
     * Hint: Check the connection ID database.
     */
	bool check_if_new_connection(const Packet *pkt);

	/*Check if the packet represent Connection reset
     * i.e., if the RST flag is set in the header.
     * Return true if connection reset
     * else return false.*/
	bool check_if_connection_reset(const Packet *pkt);

	/* Add a new connection to the map along with its action.*/
	void add_connection(Connection &c, int action);

	/* Delete the connection from map*/
	void delete_connection(Connection &c);

	/* Create a new connection object for Packet.
     * Make sure you canonicalize the source and destination ip address and port number.
     * i.e, make the source less than the destination and
     * update isfw to false if you have to swap source and destination.
     * return NULL on error. */
	Connection get_canonicalized_connection(const Packet *pkt);

	/* Read policy from a config file whose path is passed as parameter.
     * Update the policy database.
     * Policy config file structure would be space separated list of
     * <source_ip source_port destination_ip destination_port protocol action>
     * Add Policy objects to the list_of_policies
     * */
	int read_policy_config(String file_path);

	/* Convert the integer ip address to string in dotted format.
     * Store the string in s.
     *
     * Hint: ntohl could be useful.*/
	void dotted_addr(const struct in_addr* addr, char *s);


	/* Check if Packet belongs to new connection.
     * If new connection, apply the policy on this packet
     * and add the result to the connection map.
     * Else return the action in map.
     * If Packet indicates connection reset,
     * delete the connection from connection map.
     *
     * Return 1 if packet is allowed to pass
     * Return 0 if packet is to be discarded
     */
	int filter_packet(const Packet *pkt);

	/* Push valid traffic on port 1
    * Push discarded traffic on port 0*/
	void push(int port, Packet *packet);

	/*The default action configured for the firewall.*/
	int DEFAULTACTION;
};

//CLICK_ENDDECLS
#endif
