/*
 * statefulipfilter.{cc,hh} -- Stateful IP-packet filter
 *
 */
#include "../include/click/fixconfig.h"
#include <click/config.h>
#include <click/confparse.hh>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "statefulfirewall.hh"


/* Add header files as required*/
CLICK_DECLS

//Add your implementation here.
Connection Policy::getConnection()
{
    return Connection(sourceip, destip, sourceport, destport, 0, 0, 0, proto, (bool)action);
}


int StatefulFirewall::configure(Vector<String> &conf, ErrorHandler *errh)
{
    int default_action;
    String policy_file;

    if (Args(conf, this, errh).
            read_m("POLICYFILE", policy_file).
            read_m("DEFAULT", default_action).
            complete() < 0)
        return EXIT_FAILURE;

    read_policy_config(policy_file);
    DEFAULTACTION = default_action;
    return EXIT_SUCCESS;
}

void StatefulFirewall::push(int port, Packet *packet) 
{
    int action = filter_packet(packet);
    // click_chatter("Packet: port %d, length %d, action %d", port, packet->length(), action);
    if (action == 1)
    {
        output(1).push(packet);
    } else
    {
        output(0).push(packet);
    }
}

bool StatefulFirewall::check_if_connection_reset(const Packet * pkt)
{
    const click_tcp *tcp_header = pkt->tcp_header();
    return (tcp_header->th_flags & TH_RST) == TH_RST;

}

// Check if TCP packet is new connection
bool StatefulFirewall::check_if_new_connection(const Packet *pkt)
{
    Connection cur_conn = get_canonicalized_connection(pkt);
    // SYN packet, must be new
    if (cur_conn.get_handshake_stat() == SYN)
    {
        // click_chatter("Get SYN packet.");
        return true;
    }

    for(std::map<Connection,int, cmp_connection>::iterator it = Connections.begin();
        it != Connections.end(); it++)
    {
        if (cur_conn == it->first)
            return false;
    }

    return true;
}

void StatefulFirewall::add_connection(Connection &c, int action)
{
     //click_chatter("Add new connection:");
     //c.print();
    const Connection *new_conn_key = new Connection(c);
    Connections[*new_conn_key] = action;
}

void StatefulFirewall::delete_connection(Connection &c)
{
    Connections.erase(c);
}

Connection StatefulFirewall::get_canonicalized_connection(const Packet *pkt)
{
    const click_ip *ip_header = pkt->ip_header();

    char src_ip[20];
    char dst_ip[20];
    int proto = ip_header->ip_p;
    Connection cur_conn;

    dotted_addr(&ip_header->ip_src, src_ip);
    dotted_addr(&ip_header->ip_dst, dst_ip);
    String srcip_str(src_ip);
    String dstip_str(dst_ip);


    switch (proto)
    {
        case IP_PROTO_ICMP:
        {
            cur_conn = Connection(srcip_str, dstip_str, 0, 0, 0, 0, 0, IP_PROTO_ICMP, true);
        }
        break;
        case IP_PROTO_TCP:
        {
            const click_tcp *tcp_header = pkt->tcp_header();
            unsigned int ack = 0;
            if ((tcp_header->th_flags & TH_ACK) != 0)
            {
                ack = ntohl(tcp_header->th_ack);
            }
            cur_conn = Connection(srcip_str, dstip_str, ntohs(tcp_header->th_sport), ntohs(tcp_header->th_dport),
                                  (unsigned long)ntohl(tcp_header->th_seq), 0, ack, IP_PROTO_TCP, true);

            // Check if SYN packet
            int hs_state = -1;
            if ((tcp_header->th_flags & (TH_SYN | TH_ACK)) == TH_SYN)
            {
                hs_state = SYN;
            }

            cur_conn.update_handshake_stat(hs_state);
        }
        break;
        case IP_PROTO_UDP:
        {
            const click_udp *udp_header = pkt->udp_header();
            cur_conn = Connection(srcip_str, dstip_str, ntohs(udp_header->uh_sport), ntohs(udp_header->uh_dport),
                                  0, 0, 0, IP_PROTO_UDP, true);
        }
        break;
        default:
        {
            cur_conn = Connection(srcip_str, dstip_str, 0, 0,
                                  0, 0, 0, IP_PROTO_NONE, true);
        }
    }
    return cur_conn;
}

int StatefulFirewall::read_policy_config(String file_path)
{
    char sourceip[100];
    char destip[100];
    int sourceport = 0;
    int destport = 0;
    int proto = 0;
    int action = 0;

    const char *c_path = file_path.c_str();
    ifstream policy_file(c_path);

    string line;
    while (getline(policy_file, line))
    {
        if (line[0] == '#' or line.length() == 0)
            continue;
        sscanf(line.c_str(), "%s %d %s %d %d %d", sourceip, &sourceport, destip, &destport, &proto, &action);
        String sip_string(sourceip);
        String dip_string(destip);
        Policy *new_policy = new Policy(sip_string, dip_string, sourceport, destport, proto, action);
        // new_policy->getConnection().print();
        list_of_policies.push_back(*new_policy);
    }
    return EXIT_SUCCESS;
}

void StatefulFirewall::dotted_addr(const struct in_addr* addr, char *s)
{
    s[0] = '\0';
    strcpy(s, inet_ntoa(*addr));
}
/* Check if Packet belongs to new connection.
 * If new connection, apply the policy on this packet
 * and add the result to the connection map.
 * Else return the action in map.
 * If Packet indicates connection reset,
 * delete the connection from connection map.
 *
 * Return 1 if packet is allowed to pass
 * Return 0 if packet is to be discarded
 */
int StatefulFirewall::filter_packet(const Packet *pkt)
{
    int action = DEFAULTACTION;
    Connection cur_conn = get_canonicalized_connection(pkt);
    const click_ip *ip_header = pkt->ip_header();

    if (ip_header->ip_p == IP_PROTO_TCP)
    {
        // New connection
        if (check_if_new_connection(pkt))
        {
            // not SYN packet, error
            if (cur_conn.get_handshake_stat() != SYN)
                return 0;

            // Check TCP SYN flood
            for(std::map<Connection,int, cmp_connection>::iterator it = Connections.begin();
                it != Connections.end(); it++)
            {
                if ((cur_conn == it->first) and cur_conn.get_sourceseq() == it->first.get_sourceseq())
                {
                    delete_connection(cur_conn);
                    return 0;
                }
            }

            // click_chatter("It's a new connection.");
            for (int i = 0; i < list_of_policies.size(); i++)
            {
                if (list_of_policies[i].getConnection().compare(cur_conn) == 0)
                {
                    action = list_of_policies[i].getAction();
                    if (action == 1)
                    {
                        add_connection(cur_conn, action);
                    }
                    return action;
                }
            }
            if (DEFAULTACTION == 1)
                add_connection(cur_conn, 1);
        }
        else // Old connection
        {
            Connection old_conn;
            for(std::map<Connection,int, cmp_connection>::iterator it = Connections.begin();
                it != Connections.end(); it++)
            {
                if (cur_conn == it->first)
                {
                    old_conn = it->first;
                    action = it->second;
                }
            }

            // Check if request reset connection
            if (check_if_connection_reset(pkt))
            {
                delete_connection(old_conn);
                return 1;
            }

            // Check if error state connection
            int new_handshake_state = HS_DONE;
            if (old_conn.get_handshake_stat() != HS_DONE)
            {

                if (cur_conn.is_reverse(old_conn))
                {
                    if (old_conn.get_sourceseq() + 1 != cur_conn.get_ack())
                    {
                        return 0;
                    }
                }
                else {
                    if (old_conn.get_destseq() + 1 != cur_conn.get_ack()) {
                        return 0;
                    }
                }
                // Update handshake state
                new_handshake_state = old_conn.get_handshake_stat() + 1;
            }

            // Update connection
            Connection update_conn;
            if (old_conn.is_reverse(cur_conn))
            {
                update_conn = Connection(old_conn);
                update_conn.set_destseq(cur_conn.get_sourceseq());
            }
            else {
                update_conn = Connection(cur_conn);
                update_conn.set_destseq(old_conn.get_destseq());
            }
            update_conn.update_handshake_stat(new_handshake_state);
            update_conn.reset_ack();

            // Delete out-dated connection, add updated connection
            delete_connection(old_conn);
            add_connection(update_conn, action);

             // click_chatter("Update connection info:");
             // update_conn.print();
            return action;
        }
    }
    else // Non-TCP packet
    {
        // click_chatter("Receive non-tcp packet, type %x.", ip_header->ip_p);
        for (int i = 0; i < list_of_policies.size(); i++)
        {
            if (list_of_policies[i].getConnection() == cur_conn)
                return list_of_policies[i].getAction();
        }
    }

    // Apply default Action
    return DEFAULTACTION;
}


CLICK_ENDDECLS
EXPORT_ELEMENT(StatefulFirewall)
